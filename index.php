<?php 

require_once "animal.php";
require_once "Ape.php";
require_once "Frog.php";

echo "SHEEP <br>";
$sheep = new Animal("shaun");
echo $sheep->name . "<br>"; // "shaun"
echo $sheep->get_legs() . "<br>"; // 2
echo $sheep->get_cold_blooded() . "<br>"; // false

echo "<br> <br> <hr> APE <br>";
$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong->get_legs() . "<br>";
echo $sungokong->get_cold_blooded() . "<br>";
$sungokong->yell(); // "Auooo"

echo "<br> <br> <hr> FROG <br>";
$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok->get_legs() . "<br>";
echo $kodok->get_cold_blooded() . "<br>";
$kodok->jump() ; // "hop hop"

 ?>